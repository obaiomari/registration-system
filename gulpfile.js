/*=============================================
=            Gulp Starter by @zain            =
=============================================*/

/**
*
* The packages we are using
* Not using gulp-load-plugins as it is nice to see whats here.
*
**/
var gulp         = require('gulp');
var sass         = require('gulp-sass');
var less         = require('gulp-less');
//var browserSync  = require('browser-sync');
var prefix       = require('gulp-autoprefixer');
var plumber      = require('gulp-plumber');
var uglify       = require('gulp-uglifyjs');
var notify       = require("gulp-notify");
var rename       = require("gulp-rename");
var sourcemaps = require('gulp-sourcemaps');
var partialify = require('partialify');
var browserify = require('browserify');
var postcss = require('gulp-postcss');
var cssnano = require('cssnano');
var path = require('path');
var source = require('vinyl-source-stream');
var concat = require('gulp-concat');
var merge = require('merge-stream');

var config = {
    lessPath: path.join('resources', 'less'),
    scssPath: path.join('resources', 'scss'),
    cssPath: path.join('resources', 'css'),
    bowerDir: 'bower_components',
    nodeDir: 'node_modules',
    appPath: path.join('.', 'luct')
}

/**
*
* Styles
* - Compile
* - Compress/Minify
* - Catch errors (gulp-plumber)
* - Autoprefixer
*
**/

var lessPath = [
      path.join('resources','less','application.less'),
];

var scssPath = [
      path.join(config.nodeDir, 'bootstrap', 'dist', 'css', 'bootstrap.css'),
      path.join('resources','scss','main.scss'),
      path.join('resources','scss', 'font-awesome-4.7.0', 'scss','font-awesome.scss'),
];

var cssPath = [
     path.join('resources','css','bootstrap-formhelpers.min.css'),
     path.join('resources','css','intlTelInput.css'),
     path.join('resources','css','custom.css'),
];


gulp.task('css', function() {
 

     var lessStream = gulp.src(lessPath)
        .pipe(less())
        .pipe(concat('less-files.less'));

    var scssStream = gulp.src(scssPath)
        .pipe(sourcemaps.init())
        .pipe(sass())
        .pipe(concat('scss-files.scss'));
    
    var cssStream = gulp.src(cssPath)
        .pipe(concat('css-files.css'));

    var mergedStream = merge(scssStream, lessStream, cssStream)

        .pipe(concat('style.css'))
        .pipe(postcss([cssnano({discardComments: {removeAll: true}})]))
        .pipe(gulp.dest(path.join('public','css')))
        .pipe(notify("Successfully compiled CSS"))

});

/**
*
* Javascript
* - Uglify
*
**/


var jsPaths = [
//    path.join('.', 'resources', 'js', 'main.js')
    path.join(config.nodeDir, 'jquery', 'dist', 'jquery.js'),
    path.join(config.nodeDir, 'bootstrap', 'dist', 'js', 'bootstrap.js'),
    path.join('.', 'resources', 'js', 'bootstrap-formhelpers-countries.en_US.js'),
    path.join('.', 'resources', 'js', 'bootstrap-formhelpers-countries.js'),
    path.join('.', 'resources', 'js', 'bootstrap-formhelpers-selectbox.js'),
    path.join('.', 'resources', 'js', 'bootstrap-formhelpers-states.en_US.js'),
    path.join('.', 'resources', 'js', 'bootstrap-formhelpers-states.js'),
    path.join('.', 'resources', 'js', 'intlTelInput.js'),    
    path.join('.', 'resources', 'js', 'custom.js'),
 ];
/*
gulp.task('browserify', function() {
    return browserify(path.join(.,'public', 'js', 'application.js'))
        .transform(partialify)
      //  .transform(envify(process.env))
        .bundle()
        .pipe(source('application.js'))
        .pipe(gulp.dest(path.join('.', 'resources', 'js')));
})
*/
gulp.task('build-js', function() {
    var gulpSrc = gulp.src(jsPaths)
      .pipe(concat('application.js'));
    //  if(typeof process.env.APP_ENV != 'undefined' && process.env.APP_ENV == 'production') {
      gulpSrc = gulpSrc.pipe(uglify());
      //}
      gulpSrc.pipe(gulp.dest(path.join('.','public','js')))
      gulpSrc.pipe(notify("Successfully compiled JS"));
});



gulp.task('default', ['build-js', 'css'], function () {
    gulp.watch(lessPath, ['css']);
    gulp.watch(scssPath, ['css']);
    gulp.watch(cssPath, ['css']);
    gulp.watch(jsPaths,['build-js']);
});
